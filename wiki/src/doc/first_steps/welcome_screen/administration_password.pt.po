# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2024-02-26 12:27+0100\n"
"PO-Revision-Date: 2023-08-01 15:22+0000\n"
"Last-Translator: drebs <drebs@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Administration password\"]]\n"
msgstr "[[!meta title=\"Senha de administração\"]]\n"

#. type: Plain text
msgid ""
"In Tails, an administration password (also called *root password* or "
"*amnesia password*) is required to perform system administration tasks.  For "
"example:"
msgstr ""
"No Tails, uma senha de administração (também chamada de *senha de root* ou "
"*senha amnesia*) é necessária para realizar tarefas de administração de "
"sistema. Por exemplo:"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "To [[install additional software|persistent_storage/additional_software]]"
msgid ""
"- To [[install additional software|persistent_storage/additional_software]]"
msgstr ""
"Para [[instalar programas adicionais|persistent_storage/additional_software]]"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "To [[access the internal hard disks of the computer|doc/advanced_topics/"
#| "internal_hard_disk]]"
msgid ""
"- To [[access the internal hard disks of the computer|doc/advanced_topics/"
"internal_hard_disk]]"
msgstr ""
"Para [[acessar os discos rígidos internos do computador|doc/advanced_topics/"
"internal_hard_disk]]"

#. type: Plain text
#, fuzzy
#| msgid "To execute commands with <span class=\"command\">sudo</span>"
msgid "- To execute commands with `sudo`"
msgstr "Para executar comandos com <span class=\"command\">sudo</span>"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "[[!img password-prompt.png link=\"no\" alt=\"Authentication required: amnesia password (also called *administration password* or *root password*)\"]] <!-- Note for translators: the alt tag is useful for SEO. -->\n"
msgid "[[!img password-prompt.png link=\"no\" class=\"screenshot\" alt=\"Authentication required: amnesia password (also called *administration password* or *root password*)\"]] <!-- Note for translators: the alt tag is useful for SEO. -->\n"
msgstr "[[!img password-prompt.png link=\"no\" alt=\"Autenticação necessária: senha amnesia (também chamada de *senha de administração* ou *senha de root*)\"]] <!-- Note for translators: the alt tag is useful for SEO. -->\n"

#. type: Title =
#, no-wrap
msgid "Security implications of setting up an administration password"
msgstr "Implicações de segurança ao configurar uma senha de administração"

#. type: Plain text
msgid "For better security, no administration password is set up by default."
msgstr ""
"Para uma melhor segurança, nenhuma senha de administração é definida por "
"padrão."

#. type: Plain text
msgid ""
"When you set up an administration password, an attacker who can get physical "
"access to your computer while Tails is running could be able to break your "
"security and:"
msgstr ""
"Quando uma senha de administração está configurada, um atacante que tenha "
"acesso físico ao seu computador durante a execução do Tails pode minar sua "
"segurança e:"

#. type: Plain text
msgid "- Monitor all your activity."
msgstr ""

#. type: Plain text
msgid "- Reveal your IP address."
msgstr ""

#. type: Plain text
msgid "- Access all the data in your Persistent Storage."
msgstr ""

#. type: Plain text
msgid "- Read and write data on the internal hard disk of the computer."
msgstr ""

#. type: Plain text
msgid "- Install such vulnerabilities permanently on your Tails USB stick."
msgstr ""

#. type: Plain text
msgid ""
"To learn your administration password, an attacker could watch your keyboard "
"while you type, for example looking over your shoulders or through a video "
"surveillance camera."
msgstr ""
"Para conseguir a senha de administração, um atacante poderia observar o "
"teclado enquanto você digita, olhando por exemplo por sobre os seus ombro ou "
"através de uma câmera de vigilância."

#. type: Plain text
msgid "To prevent such an attack:"
msgstr "Para prevenir um ataque desse tipo:"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "Hide your keyboard while typing your administration password in public "
#| "spaces."
msgid ""
"- Hide your keyboard while typing your administration password in public "
"spaces."
msgstr ""
"Esconda seu teclado ao escrever sua senha de administração quando estiver em "
"espaços públicos."

#. type: Plain text
#, fuzzy
#| msgid "Never leave your computer unattended while running Tails."
msgid "- Never leave your computer unattended while running Tails."
msgstr "Nunca deixe seu computador abandonado enquanto estiver rodando Tails."

#. type: Plain text
#, fuzzy
#| msgid "Learn how to [[quickly shut down Tails|doc/first_steps/shutdown]]."
msgid ""
"- Learn how to [[shut down Tails quickly|doc/first_steps/shutdown]] in case "
"of emergency."
msgstr ""
"Aprenda como [[desligar o Tails rapidamente|doc/first_steps/shutdown]]."

#. type: Title =
#, no-wrap
msgid "Set up an administration password"
msgstr "Configure uma senha de administração"

#. type: Plain text
msgid ""
"In order to perform administration tasks, you need to set up an "
"administration password when starting Tails, using the [[Welcome Screen|"
"welcome_screen]]."
msgstr ""
"Para poder realizar tarefas de administração, você precisa configurar uma "
"senha de administração ao iniciar o Tails, usando a [[Tela de Boas-vindas|"
"welcome_screen]]."

#. type: Bullet: '1. '
msgid ""
"When the Welcome Screen appears, click on the **[[!img lib/list-add.png "
"alt=\"Add Additional Setting\" class=\"symbolic\" link=\"no\"]]** button."
msgstr ""
"Quando a Tela de Boas-vindas aparecer, clique no botão **[[!img lib/list-add."
"png alt=\"Adicionar Configuração Adicional\" class=\"symbolic\" "
"link=\"no\"]]**."

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "   [[!img additional.png link=\"no\" alt=\"\"]]\n"
msgid "   [[!img additional.png link=\"no\" class=\"screenshot\" alt=\"\"]]\n"
msgstr "   [[!img additional.png link=\"no\" alt=\"\"]]\n"

#. type: Bullet: '2. '
msgid ""
"Choose **Administration Password** in the **Additional Settings** dialog."
msgstr ""
"Escolha **Senha de Administração** na janela **Configurações Adicionais**."

#. type: Bullet: '3. '
msgid ""
"Specify a password of your choice in both the <span "
"class=\"guilabel\">Administration Password</span> and <span "
"class=\"guilabel\">Confirm</span> text boxes then click <span "
"class=\"bold\">Add</span>."
msgstr ""
"Especifique uma senha de sua escolha nas caixas de texto <span "
"class=\"guilabel\">Senha de administração</span> e <span "
"class=\"guilabel\">Confirmar</span>, e então clique <span "
"class=\"bold\">Add</span>."

#. type: Plain text
#, no-wrap
msgid "<div class=\"note\">\n"
msgstr "<div class=\"note\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>For security, it is impossible to set up an administration password after\n"
"starting Tails. Otherwise, a compromised application could bypass some of the\n"
"security built in Tails.</p>\n"
msgstr ""
"<p>Por segurança, não é possível configurar uma senha de administração após\n"
"o início do Tails. Se isso fosse possível, um aplicativo comprometido poderia\n"
"driblar parte da segurança embutida no Tails.</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"open_root_terminal\"></a>\n"
msgstr "<a id=\"open_root_terminal\"></a>\n"

#. type: Title =
#, no-wrap
msgid "How to open a root terminal"
msgstr "Como abrir um terminal de root"

#. type: Plain text
msgid ""
"To open a root terminal during your working session, you can do any of the "
"following:"
msgstr ""
"Para abrir um terminal de root durante sua sessão de trabalho, você pode "
"fazer qualquer um dos seguintes:"

#. type: Plain text
msgid ""
"- Choose **Applications**&nbsp;▸ **System Tools**&nbsp;▸ **Root Terminal**."
msgstr ""

#. type: Plain text
#, fuzzy
#| msgid "Execute <span class=\"command\">sudo -i</span> in a terminal."
msgid "- Execute the command `sudo -i` in a terminal."
msgstr ""
"Execute comandos com <span class=\"command\">sudo -i</span> em um terminal."

#~ msgid ""
#~ "- Monitor all your activity - Reveal your IP address - Access all the "
#~ "data in your Persistent Storage - Read and write data on the internal "
#~ "hard disk of the computer - Install such vulnerabilities permanently on "
#~ "your Tails USB stick"
#~ msgstr ""
#~ "- Monitorar toda sua atividade\n"
#~ "- Descobrir seu endereço IP real\n"
#~ "- Acessar todos os dados no seu Armazenamento Persistente\n"
#~ "- Ler e escrever dados no disco rígido interno do computador\n"
#~ "- Instalar estas vulnerabilidades de forma permanente no seu pendrive USB "
#~ "com Tails"

#~ msgid ""
#~ "- Hide your keyboard while typing your administration password in public "
#~ "spaces.  - Never leave your computer unattended while running Tails.  - "
#~ "Learn how to [[shut down Tails quickly|doc/first_steps/shutdown]] in case "
#~ "of emergency."
#~ msgstr ""
#~ "- Esconda seu teclado ao digitar sua senha de administração em espaços "
#~ "públicos.  - Nunca largue seu computador sozinho enquanto estiver rodando "
#~ "o Tails.  - Aprenda como [[desligar o Tails de forma rápida|doc/"
#~ "first_steps/shutdown]] em caso de emergência."

#, no-wrap
#~ msgid ""
#~ "  - Choose\n"
#~ "    <span class=\"menuchoice\">\n"
#~ "      <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
#~ "      <span class=\"guisubmenu\">System Tools</span>&nbsp;▸\n"
#~ "      <span class=\"guimenuitem\">Root Terminal</span></span>.\n"
#~ msgstr ""
#~ "  - Escolha\n"
#~ "    <span class=\"menuchoice\">\n"
#~ "      <span class=\"guimenu\">Aplicativos</span>&nbsp;▸\n"
#~ "      <span class=\"guisubmenu\">Ferramentas de sistema</span>&nbsp;▸\n"
#~ "      <span class=\"guimenuitem\">Root Terminal</span></span>.\n"

#~ msgid "To learn your administration password, an attacker could:"
#~ msgstr "Para descobrir sua senha de administração, um atacante poderia:"

#~ msgid ""
#~ "Watch your keyboard while you type your administration password, for "
#~ "example through a video surveillance camera, and then accessing "
#~ "physically your computer before you shut down Tails."
#~ msgstr ""
#~ "Observar seu teclado enquanto você escreve a sua senha de administração "
#~ "através, por exemplo, de uma câmera de segurança, e então conseguir "
#~ "acesso físico ao seu computador antes que você desligue o Tails."

#~ msgid ""
#~ "Exploit a security vulnerability in an application in Tails before you "
#~ "type your administration password."
#~ msgstr ""
#~ "Explorar uma falha de segurança em algum aplicativo do Tails antes de "
#~ "você digitar a sua senha de administração."

#, no-wrap
#~ msgid ""
#~ "  For example, an attacker could exploit a security vulnerability in\n"
#~ "  *Thunderbird* by sending you a [phishing\n"
#~ "  email](https://ssd.eff.org/en/module/how-avoid-phishing-attacks) that\n"
#~ "  could wait until you type your administration password and reveal it.\n"
#~ msgstr ""
#~ "  Por exemplo, um atacante pode explorar uma vulnerabilidade no\n"
#~ "  *Thunderbird* ao mandar um [email de\n"
#~ "  phishing](https://ssd.eff.org/en/module/how-avoid-phishing-attacks) que\n"
#~ "  poderia esperar até que você digitasse a senha de administração e assim obtê-la.\n"

#, no-wrap
#~ msgid ""
#~ "  Such an attack is very unlikely but could be performed by a strong\n"
#~ "  attacker, such as a government or a hacking firm.\n"
#~ msgstr ""
#~ "  Um ataque desse tipo é bem improvável, mas pode ser realizado por um atacante\n"
#~ "  forte, tais como um governo ou uma empresa de hacking.\n"

#~ msgid "Only set up an administration password when you need it."
#~ msgstr "Apenas configure uma senha de administração quando for necessário."

#~ msgid ""
#~ "Always update to the latest version of Tails to fix known vulnerabilities "
#~ "as soon as possible."
#~ msgstr ""
#~ "Sempre atualize para a versão mais recente do Tails para corrigir as "
#~ "vulnerabilidades conhecidas o mais rápido possível."

#, no-wrap
#~ msgid ""
#~ "  We have plans to fix the root cause of this problem but it requires\n"
#~ "  [[!tails_ticket 12213 desc=\"important engineering work\"]].\n"
#~ msgstr ""
#~ "  Temos planos para consertar a raiz desse problema, mas isso requer um\n"
#~ "  [[!tails_ticket 12213 desc=\"trabalho importante de engenharia\"]].\n"

#~ msgid ""
#~ "**By default, the administration password is disabled for better security."
#~ "**\n"
#~ "This can prevent an attacker with physical or remote access to your Tails "
#~ "system\n"
#~ "to gain administration privileges and perform administration tasks\n"
#~ "against your will.\n"
#~ msgstr ""
#~ "**Por padrão, a senha de administração é desabilitada para maior "
#~ "segurança.**\n"
#~ "Isto pode evitar que um atacante com acesso físico ou remoto ao seu "
#~ "sistema Tails\n"
#~ "ganhe privilégios administrativos e execute tarefas de administração sem "
#~ "seu\n"
#~ "conhecimento.\n"

#~ msgid "To install new programs and packages"
#~ msgstr "Para instalar novos programas e pacotes"

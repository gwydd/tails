# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-01-31 15:45+0100\n"
"PO-Revision-Date: 2024-02-29 20:45+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta robots=\"noindex\"]]\n"
msgstr "[[!meta robots=\"noindex\"]]\n"

#. type: Plain text
msgid ""
"If your Wi-Fi interface is not working, you might experience the following:"
msgstr ""
"Si la vostra interfície Wi-Fi no funciona, és possible que experimenteu el "
"següent:"

#. type: Bullet: '* '
msgid "There is no **Wi-Fi** menu in the system menu:"
msgstr "No hi ha cap menú de **Wi-Fi** al menú del sistema:"

#. type: Plain text
#, no-wrap
msgid "  [[!img no-wifi/system_without_wifi.png link=\"no\" class=\"screenshot\" alt=\"\"]]\n"
msgstr ""
"  [[!img no-wifi/system_without_wifi.png link=\"no\" class=\"screenshot\" "
"alt=\"\"]]\n"

#. type: Bullet: '* '
msgid "The interface is disabled because MAC address anonymization failed:"
msgstr ""
"La interfície està desactivada perquè l'anonimització de l'adreça MAC ha "
"fallat:"

#. type: Plain text
#, no-wrap
msgid "  [[!img no-wifi/mac_spoofing_disabled.png link=\"no\" class=\"screenshot\" alt=\"Notification: MAC address anonymization failed\"]]\n"
msgstr ""
"  [[!img no-wifi/mac_spoofing_disabled.png link=\"no\" class=\"screenshot\" "
"alt=\"Notificació: l'anonimització de l'adreça MAC ha fallat\"]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"  In this case, you can disable MAC address anonymization in the Welcome\n"
"  Screen. Doing so has security implications, so read carefully our\n"
"  [[documentation about MAC address\n"
"  anonymization|first_steps/welcome_screen/mac_spoofing]].\n"
msgstr ""
"  En aquest cas, podeu desactivar l'anonimització de l'adreça MAC a la "
"Pantalla de Benvinguda.\n"
"  Fer-ho té implicacions de seguretat, així que llegiu\n"
"  acuradament la nostra [[documentació sobre\n"
"  l'anonimització de l'adreça MAC|first_steps/welcome_screen/mac_spoofing]]."
"\n"

#. type: Bullet: '* '
msgid "The following notification appears:"
msgstr "Apareix la següent notificació:"

#. type: Plain text
#, no-wrap
msgid "      Activation of network connection failed\n"
msgstr "      L'activació de la connexió a la xarxa ha fallat\n"

#. type: Plain text
msgid "To connect to the Internet, you can try to:"
msgstr "Per connectar-vos a Internet, podeu provar de:"

#. type: Bullet: '* '
msgid ""
"Use an Ethernet cable instead of Wi-Fi if possible. Wired interfaces work "
"much more reliably than Wi-Fi in Tails."
msgstr ""
"Si és possible, utilitzeu un cable Ethernet en lloc de Wi-Fi. Les "
"interfícies per cable funcionen de manera molt més fiable que la Wi-Fi a "
"Tails."

#. type: Bullet: '* '
msgid ""
"Share the Wi-Fi or mobile data connection of your phone using a USB cable. "
"Sharing a connection this way is called *USB tethering*."
msgstr ""
"Compartiu la connexió Wi-Fi o de dades mòbils del vostre telèfon mitjançant "
"un cable USB. Compartir una connexió d'aquesta manera s'anomena *ancoratge "
"USB*."

#. type: Plain text
#, no-wrap
msgid "  [[!img no-wifi/usb-tethering.png link=\"no\" alt=\"\"]]\n"
msgstr "  [[!img no-wifi/usb-tethering.png link=\"no\" alt=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "  See instructions for:\n"
msgstr "  Consulteu les instruccions per a:\n"

#. type: Bullet: '  * '
msgid "[iPhones or iPads](https://support.apple.com/en-us/HT204023)"
msgstr "[iPhones o iPads](https://support.apple.com/en-us/HT204023)"

#. type: Plain text
#, no-wrap
msgid "    Only sharing mobile data works on iPhones and iPads; sharing Wi-Fi does not work.\n"
msgstr "    L'ús compartit de dades mòbils funciona en iPhones i iPads; compartir Wi-Fi no funciona.\n"

#. type: Bullet: '  * '
msgid "[Android](https://support.google.com/android/answer/9059108?hl=en)"
msgstr "[Android](https://support.google.com/android/answer/9059108?hl=ca)"

#. type: Plain text
#, no-wrap
msgid ""
"  <div class=\"caution\">\n"
"  <p>Tails cannot hide the information that identifies your phone on the local\n"
"  network. If you connect your phone to a:</p>\n"
"  <ul>\n"
"  <li><p>Wi-Fi network: the network can see the MAC\n"
"  address of your phone.</p>\n"
"  <p>This has security implications that we explain in our [[documentation on\n"
"  MAC address anonymization|first_steps/welcome_screen/mac_spoofing]].  Some\n"
"  phones have a feature to hide the MAC address of the phone.</p></li>\n"
"  <li><p>Mobile data network: the network is able to know the identifier of\n"
"  your SIM card (IMSI) and the serial number of your phone (IMEI).</p></li>\n"
"  </ul>\n"
"  </div>\n"
msgstr ""
"  <div class=\"caution\">\n"
"  <p>Tails no pot amagar la informació que identifica el vostre telèfon a "
"la\n"
"  xarxa local. Si connecteu el vostre telèfon a:</p>\n"
"  <ul>\n"
"  <li><p>Una xarxa Wi-Fi: la xarxa pot veure l'adreça MAC\n"
"  del vostre telèfon.</p>\n"
"  <p>Això té implicacions de seguretat que expliquem a la nostra [["
"documentació sobre\n"
"  l'anonimització de l'adreça MAC|first_steps/welcome_screen/mac_spoofing]]. "
"Alguns\n"
"  telèfons tenen una funcionalitat per ocultar l'adreça MAC del "
"telèfon.</p></li>\n"
"  <li><p>Una xarxa de dades mòbils: la xarxa és capaç de conèixer "
"l'identificador de\n"
"  la vostra targeta SIM (IMSI) i el número de sèrie del vostre telèfon "
"(IMEI).</p></li>\n"
"  </ul>\n"
"  </div>\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"wifi-adapters\"></a>\n"
msgstr "<a id=\"wifi-adapters\"></a>\n"

#. type: Bullet: '* '
msgid "Buy a USB Wi-Fi adapter that works in Tails:"
msgstr "Compreu un adaptador USB Wi-Fi que funcioni a Tails:"

#. type: Plain text
#, no-wrap
msgid "  <!--  <tr><td>D-Link</td><td>DWA-121</td><td>Nano</td><td>150 Mbit/s</td><td>$6</td><td>No</td><td><a href=\"https://www.amazon.com/d/B004X8R7HY\">Amazon</a></td></tr>-->\n"
msgstr "  <!--  <tr><td>D-Link</td><td>DWA-121</td><td>Nano</td><td>150 Mbit/s</td><td>6$</td><td>No</td><td><a href=\"https://www.amazon.com/d/B004X8R7HY\">Amazon</a></td></tr>-->\n"

#. type: Plain text
#, no-wrap
msgid ""
"  <table>\n"
"  <tr><th>Vendor</th><th>Model</th><th>Size</th><th>Speed</th><th>Price</th><th>Buy offline</th><th>Buy online</th></tr>\n"
"  <tr><td>Panda Wireless</td><td>Ultra</td><td>Nano</td><td>150 Mbit/s</td><td>$18</td><td>No</td><td><a href=\"https://www.amazon.com/d/B00762YNMG\">Amazon</a></td></tr> <!-- 148f:5370 -->\n"
"  <tr><td>Panda Wireless</td><td>PAU05</td><td>Small</td><td>300 Mbit/s</td><td>$65</td><td>No</td><td><a href=\"https://www.amazon.com/d/B00EQT0YK2\">Amazon</a></td></tr> <!-- 148f:5372 -->\n"
"  <tr><td>ThinkPenguin</td><td>TPE-N150USB</td><td>Nano</td><td>150 Mbit/s</td><td>$74</td><td>No</td><td><a href=\"https://www.thinkpenguin.com/gnu-linux/penguin-wireless-n-usb-adapter-gnu-linux-tpe-n150usb\">ThinkPenguin</a></td></tr> <!-- ????:???? -->\n"
"  </table>\n"
msgstr ""
"  <table>\n"
"  <tr><th>Proveïdor</th><th>Model</th><th>Mida</th><th>Velocitat</th><th>Preu</th><th>Compra fora de línia</th><th>Compra en línia</th></tr>\n"
"  <tr><td>Panda Wireless</td><td>Ultra</td><td>Nano</td><td>150 Mbit/s</td><td>18$</td><td>No</td><td><a href=\"https://www.amazon.com/d/B00762YNMG\">Amazon</a></td></tr> <!-- 148f:5370 -->\n"
"  <tr><td>Panda Wireless</td><td>PAU05</td><td>Petit</td><td>300 Mbit/s</td><td>65$</td><td>No</td><td><a href=\"https://www.amazon.com/d/B00EQT0YK2\">Amazon</a></td></tr> <!-- 148f:5372 -->\n"
"  <tr><td>ThinkPenguin</td><td>TPE-N150USB</td><td>Nano</td><td>150 Mbit/s</td><td>74$</td><td>No</td><td><a href=\"https://www.thinkpenguin.com/gnu-linux/penguin-wireless-n-usb-adapter-gnu-linux-tpe-n150usb\">ThinkPenguin</a></td></tr> <!-- ????:???? -->\n"
"  </table>\n"

#. type: Plain text
#, no-wrap
msgid "  <!-- See https://gitlab.tails.boum.org/tails/blueprints/-/wikis/wi-fi_adapters/ for Wi-Fi adapters that don't work or that have no advantage over these ones. -->\n"
msgstr "  <!-- Vegeu https://gitlab.tails.boum.org/tails/blueprints/-/wikis/wi-fi_adapters/ per als adaptadors Wi-Fi que no funcionen o que no tenen cap avantatge respecte aquests. -->\n"

#. type: Plain text
#, no-wrap
msgid ""
"  <div class=\"note\">\n"
"  <p>If you find another USB Wi-Fi adapter that works in Tails, please let us\n"
"  know. You can write to [[tails-support-private@boum.org]] (private email).</p>\n"
" </div>\n"
msgstr ""
"  <div class=\"note\">\n"
"  <p>Si trobeu un altre adaptador Wi-Fi USB que funcioni a Tails, feu-nos-ho saber.\n"
"  Podeu escriure a [[tails-support-private@boum.org]] (correu electrònic privat).</p>\n"
" </div>\n"

#. type: Bullet: '* '
msgid ""
"Check in our list of [[known Wi-Fi issues|support/known_issues#problematic-"
"wifi]] if there is a workaround to get your Wi-Fi interface to work in Tails."
msgstr ""
"Consulteu a la nostra llista de [[problemes coneguts de Wi-Fi|support/"
"known_issues#problematic-wifi]] si hi ha una solució alternativa per fer que "
"la vostra interfície Wi-Fi funcioni a Tails."

#~ msgid ""
#~ "The interface is disabled when starting Tails or when plugging in your "
#~ "USB Wi-Fi adapter:"
#~ msgstr ""
#~ "La interfície està desactivada en iniciar Tails o en connectar "
#~ "l'adaptador USB Wi-Fi:"

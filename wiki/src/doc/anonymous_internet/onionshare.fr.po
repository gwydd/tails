# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Tails\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2024-02-26 12:27+0100\n"
"PO-Revision-Date: 2021-10-09 16:28+0000\n"
"Last-Translator: nihei <nihei@disroot.org>\n"
"Language-Team: Tails translators <tails@boum.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.11.3\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "[[!meta title=\"Sharing files with OnionShare\"]]\n"
msgid "[[!meta title=\"Sharing files, websites, and chat rooms using OnionShare\"]]\n"
msgstr "[[!meta title=\"Partager des fichiers avec OnionShare\"]]\n"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "[OnionShare](https://onionshare.org/) is a tool to share files of any "
#| "size securely and anonymously."
msgid ""
"[*OnionShare*](https://onionshare.org/) allows you to create secure, "
"anonymous, and disposable services that other people can connect to."
msgstr ""
"[OnionShare](https://onionshare.org/) est un outil pour partager des "
"fichiers de toutes tailles de façon sûre et anonyme."

#. type: Plain text
msgid ""
"To start *OnionShare*, choose **Applications**&nbsp;▸ **Internet**&nbsp;▸ "
"**OnionShare**."
msgstr ""

#. type: Plain text
msgid ""
"From the welcome screen of *OnionShare*, you can choose between 3 different "
"services:"
msgstr ""

#. type: Plain text
msgid "- Share files - Receive files - Host a website"
msgstr ""

#. type: Plain text
msgid ""
"All these services are provided as Tor onion services and are only available "
"through the Tor network. You can share the address of these onion services "
"with other people to create temporary and anonymous communication channels."
msgstr ""

#. type: Plain text
msgid ""
"People who know their onion addresses can connect to these services from "
"*Tor Browser*, either in Tails or outside of Tails."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"tip\">\n"
msgstr "<div class=\"tip\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>Unlike most Internet services, people using your <i>OnionShare</i> services\n"
"connect directly to your Tails over the Tor network. Your data is never shared\n"
"with or stored on any third-party server. Your Tails becomes a temporary and\n"
"anonymous server.</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "   The files are not shared anymore as soon as you close\n"
#| "   <span class=\"application\">OnionShare</span> or shut down Tails.\n"
msgid ""
"These services become unavailable as soon as you close *OnionShare* or shut "
"down Tails."
msgstr ""
"   Les fichiers ne sont plus partagés dès que vous fermez\n"
"   <span class=\"application\">OnionShare</span> ou éteignez Tails.\n"

#. type: Title =
#, no-wrap
msgid "Sharing files"
msgstr ""

#. type: Plain text
msgid "With the Share service of *OnionShare*:"
msgstr ""

#. type: Plain text
msgid ""
"- You select in *OnionShare* some files from your Tails that you want to "
"share."
msgstr ""

#. type: Bullet: '- '
msgid ""
"People with the onion address can download these files from their *Tor "
"Browser*."
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "<div class=\"tip\">\n"
msgid "<div class=\"note\">\n"
msgstr "<div class=\"tip\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>Since Tails 6.0 (February 2024), the shortcut the share files via\n"
"<i>OnionShare</i> directly from the <i>Files</i> browser is not available\n"
"anymore.</p>\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Receiving files"
msgstr ""

#. type: Plain text
msgid "With the Receive service of *OnionShare*:"
msgstr ""

#. type: Bullet: '- '
msgid ""
"People with the onion address can upload files directly to this folder or "
"send you text messages from their *Tor Browser*."
msgstr ""

#. type: Bullet: '- '
msgid ""
"Received files are stored in a folder with today's date as name (for example "
"*2024-02-06*) inside the *Downloads* folder."
msgstr ""

#. type: Title =
#, no-wrap
msgid "Hosting a website"
msgstr ""

#. type: Plain text
msgid "With the Website service of *OnionShare*:"
msgstr ""

#. type: Plain text
msgid ""
"- You specify in *OnionShare* a folder that contains HTML pages in your "
"Tails."
msgstr ""

#. type: Bullet: '- '
msgid ""
"People with the onion address can visit these HTML pages as a website from "
"their *Tor Browser*."
msgstr ""

#, fuzzy, no-wrap
#~| msgid "   [[!img onionshare.png link=\"no\" alt=\"\"]]\n"
#~ msgid "[[!img services.png link=\"no\" class=\"screenshot\" alt=\"\"]]\n"
#~ msgstr "   [[!img onionshare.png link=\"no\" alt=\"\"]]\n"

#, fuzzy, no-wrap
#~| msgid "   [[!img onionshare.png link=\"no\" alt=\"\"]]\n"
#~ msgid "[[!img share.png class=\"screenshot\" alt=\"\" size=\"719x\"]]\n"
#~ msgstr "   [[!img onionshare.png link=\"no\" alt=\"\"]]\n"

#, fuzzy, no-wrap
#~| msgid "   [[!img onionshare.png link=\"no\" alt=\"\"]]\n"
#~ msgid "[[!img chat.png class=\"screenshot\" alt=\"\" size=\"719x\"]]\n"
#~ msgstr "   [[!img onionshare.png link=\"no\" alt=\"\"]]\n"

#, no-wrap
#~ msgid ""
#~ "<span class=\"application\">OnionShare</span> creates an onion address\n"
#~ "that someone else can visit in <span class=\"application\">Tor\n"
#~ "Browser</span> to access the files that you want to share.\n"
#~ msgstr ""
#~ "<span class=\"application\">OnionShare</span> créé une adresse onion\n"
#~ "qu'une autre personne peut consulter avec le <span class=\"application\">Navigateur\n"
#~ "Tor</span> pour accéder aux fichiers que vous voulez partager.\n"

#, no-wrap
#~ msgid "To share files with <span class=\"application\">OnionShare</span>:\n"
#~ msgstr "Pour partager des fichiers avec <span class=\"application\">OnionShare</span> :\n"

#~ msgid "Open the <span class=\"application\">Files</span> browser."
#~ msgstr ""
#~ "Ouvrez le navigateur de <span class=\"application\">Fichiers</span>."

#~ msgid ""
#~ "Right-click (on Mac, click with two fingers) on the files or folders that "
#~ "you want to share and choose <span class=\"guilabel\">Share via "
#~ "OnionShare</span>.  <span class=\"application\">OnionShare</span> starts."
#~ msgstr ""
#~ "Faites un clic droit (sur Mac cliquez avec deux doigts) sur les fichiers "
#~ "ou dossiers que vous voulez partager et choisissez <span "
#~ "class=\"guilabel\">Share via OnionShare</span>. <span "
#~ "class=\"application\">OnionShare</span> démarre."

#~ msgid "To share more files or folders, you can either:"
#~ msgstr ""
#~ "Pour partager plusieurs fichiers ou dossiers, vous pouvez au choix :"

#~ msgid ""
#~ "Drag and drop them from the <span class=\"application\">Files</span> "
#~ "browser onto <span class=\"application\">OnionShare</span>."
#~ msgstr ""
#~ "Les glisser-déposer depuis le navigateur de <span "
#~ "class=\"application\">Fichiers</span> vers <span "
#~ "class=\"application\">OnionShare</span>."

#~ msgid ""
#~ "Click the <span class=\"guilabel\">Add</span> button in <span "
#~ "class=\"application\">OnionShare</span>."
#~ msgstr ""
#~ "Cliquer sur le bouton <span class=\"guilabel\">Ajouter</span> dans <span "
#~ "class=\"application\">OnionShare</span>."

#~ msgid "Click <span class=\"guilabel\">Start Sharing</span>."
#~ msgstr "Cliquez sur <span class=\"guilabel\">Démarrer le serveur</span>."

#, no-wrap
#~ msgid ""
#~ "   <div class=\"bug\">\n"
#~ "   <p>Starting to share the files can take up to several minutes.</p>\n"
#~ "   </div>\n"
#~ msgstr ""
#~ "   <div class=\"bug\">\n"
#~ "   <p>Le démarrage du partage de fichiers peut prendre jusqu'à plusieurs minutes.</p>\n"
#~ "   </div>\n"

#, no-wrap
#~ msgid ""
#~ "   When the files are available for sharing, an onion address appears at\n"
#~ "   the bottom of <span class=\"application\">OnionShare</span>.\n"
#~ msgstr ""
#~ "   Lorsque les fichiers sont disponibles pour le partage, une adresse onion apparaît en\n"
#~ "   bas de <span class=\"application\">OnionShare</span>.\n"

#~ msgid "Send this onion address to someone else, for example, by email."
#~ msgstr ""
#~ "Envoyez cette adresse onion à une autre personne, par exemple, par "
#~ "courrier électronique."

#, no-wrap
#~ msgid ""
#~ "   The other person can download the files by visiting the onion address\n"
#~ "   in <span class=\"application\">Tor Browser</span>.\n"
#~ msgstr ""
#~ "   L'autre personne peut télécharger les fichiers en consultant l'adresse onion\n"
#~ "   avec le <span class=\"application\">Navigateur Tor</span>.\n"

#~ msgid ""
#~ "<span class=\"application\">OnionShare</span> informs you when the files "
#~ "are being accessed."
#~ msgstr ""
#~ "<span class=\"application\">OnionShare</span> vous informe lorsque les "
#~ "fichiers sont en cours de téléchargement."

#, no-wrap
#~ msgid ""
#~ "<p>To learn more about how to use <i>OnionShare</i>, see the\n"
#~ "<a href=\"https://docs.onionshare.org/\"><i>OnionShare</i> documentation.</p>\n"
#~ msgstr ""
#~ "<p>Pour en savoir plus sur la façon d'utiliser <i>OnionShare</i>, voir la\n"
#~ "documentation d'<a href=\"https://docs.onionshare.org/\"><i>OnionShare</i>.</p>\n"

#, no-wrap
#~ msgid ""
#~ "   <div class=\"tip\">\n"
#~ "   <p>By default, the files can only be downloaded once. To allow more\n"
#~ "   than one download:</p>\n"
#~ msgstr ""
#~ "   <div class=\"tip\">\n"
#~ "   <p>Par défaut, les fichiers peuvent être téléchargés une seule fois. Pour permettre\n"
#~ "   plus d'un téléchargement :</p>\n"

#, no-wrap
#~ msgid ""
#~ "   <ol>\n"
#~ "   <li>Click the [[!img lib/emblem-system.png link=\"no\" class=\"symbolic\"\n"
#~ "   alt=\"Show site information\"]] button to open the settings of\n"
#~ "   <span class=\"application\">OnionShare</span>.</li>\n"
#~ "   <li>Deselect the <span class=\"guilabel\">Stop sharing after first\n"
#~ "   download</span>.</li>\n"
#~ "   <li>Click <span class=\"bold\">Save</span>.</li>\n"
#~ "   </ol>\n"
#~ msgstr ""
#~ "   <ol>\n"
#~ "   <li>Cliquez sur le bouton [[!img lib/emblem-system.png link=\"no\" class=\"symbolic\"\n"
#~ "   alt=\"Montrer les informations du site\"]] pour ouvrir les paramètres de\n"
#~ "   <span class=\"application\">OnionShare</span>.</li>\n"
#~ "   <li>Désélectionnez <span class=\"guilabel\">Stop sharing after first\n"
#~ "   download</span>.</li>\n"
#~ "   <li>Cliquez sur <span class=\"bold\">Save</span>.</li>\n"
#~ "   </ol>\n"

#~ msgid ""
#~ "Tails includes [OnionShare](https://onionshare.org/), a tool for "
#~ "anonymous filesharing. It allows you to share files directly from your "
#~ "running Tails.  To do so, it creates an onion service (a website whose "
#~ "address ends with *.onion*)  within the Tor network. Any Tor user who you "
#~ "give this address to can download the files you decided to share."
#~ msgstr ""
#~ "Tails inclut [OnionShare](https://onionshare.org/), un outil de partage "
#~ "de fichiers anonyme. Il vous permet de partager des fichiers directement "
#~ "depuis Tails. Pour ce faire, il crée un service oignon (un site web dont "
#~ "l'adresse se termine par *.onion*) au sein du réseau Tor. N’importe "
#~ "quelle personne utilisant Tor à qui vous donnez cette adresse peut "
#~ "télécharger les fichiers que vous avez décidé de partager."

#~ msgid "Start the [[file browser|doc/first_steps/desktop#nautilus]]."
#~ msgstr ""
#~ "Démarrez le [[gestionnaire de fichiers|doc/first_steps/desktop#nautilus]]."

#~ msgid ""
#~ "If you want to allow multiple downloads, uncheck the <span "
#~ "class=\"guilabel\">Stop sharing automatically</span> check box.  "
#~ "Otherwise <span class=\"application\">OnionShare</span> will stop sharing "
#~ "files after they have been downloaded once."
#~ msgstr ""
#~ "Si vous voulez permettre des téléchargements multiples, décochez la case "
#~ "<span class=\"guilabel\">Arrêter le serveur automatiquement</span>. "
#~ "Autrement, <span class=\"application\">OnionShare</span> arrêtera de "
#~ "partager les fichiers après qu'ils aient été téléchargés une fois."

#~ msgid ""
#~ "Click on <span class=\"guilabel\">Start Sharing</span>.  When the files "
#~ "are available, an address similar to *http://bwwijokny5qplq5q.onion/assam-"
#~ "cover* will be shown."
#~ msgstr ""
#~ "Cliquez sur <span class=\"guilabel\">Démarrer le serveur</span>. Lorsque "
#~ "les fichiers sont accessibles, une adresse similaire à *http://"
#~ "bwwijokny5qplq5q.onion/assam-cover* s'affiche."

# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2019-06-27 16:34+0000\n"
"PO-Revision-Date: 2022-07-09 16:07+0000\n"
"Last-Translator: gallium69 <gallium69@riseup.net>\n"
"Language-Team: ita <transitails@inventati.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
msgid ""
"The BitTorrent files, signatures, and corresponding package lists [can be "
"found here](/torrents/files/)."
msgstr ""
"I file BitTorrent, le firme e la lista dei pacchetti corrispondenti [si "
"trovano qui](/torrents/files/)."

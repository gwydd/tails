# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2024-02-26 12:26+0100\n"
"PO-Revision-Date: 2024-02-28 19:50+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Tails 5.16\"]]\n"
msgstr "[[!meta title=\"Tails 5.16\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Mon, 6 Aug 2023 12:34:56 +0000\"]]\n"
msgstr "[[!meta date=\"Mon, 6 Aug 2023 12:34:56 +0000\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr "[[!pagetemplate template=\"news.tmpl\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag announce]]\n"
msgstr "[[!tag announce]]\n"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"changes\">Changes and updates</h1>\n"
msgstr "<h1 id=\"changes\">Canvis i actualitzacions</h1>\n"

#. type: Plain text
msgid ""
"- Update *Tor Browser* to [12.5.2](https://blog.torproject.org/new-release-"
"tor-browser-1252/)."
msgstr ""
"- S'ha actualitzat el *Navegador Tor* a la versió [12.5.2](https://blog."
"torproject.org/new-release-tor-browser-1252/)."

#. type: Plain text
msgid ""
"- Update *Thunderbird* to [102.14.0](https://www.thunderbird.net/en-US/"
"thunderbird/102.14.0/releasenotes/)."
msgstr ""
"- S'ha actualitzat *Thunderbird* a la versió [102.14.0](https://www."
"thunderbird.net/en-US/thunderbird/102.14.0/releasenotes/)."

#. type: Bullet: '- '
msgid ""
"Display the passphrase recommendation for the Persistent Storage in small "
"caps only."
msgstr ""
"Es mostra la recomanació de contrasenya per a l'Emmagatzematge Persistent "
"només en minúscules."

#. type: Plain text
#, no-wrap
msgid "  Capitalizing each word is more cumbersom and not more secure.\n"
msgstr "  Posar en majúscula cada paraula és més complicat i no és més segur.\n"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"fixes\">Fixed problems</h1>\n"
msgstr "<h1 id=\"fixes\">Problemes solucionats</h1>\n"

#. type: Plain text
msgid ""
"- Fix some failures while unlocking the Persistent Storage. ([[!tails_ticket "
"19728]])"
msgstr ""
"- S'han solucionat alguns errors de desbloqueig de l'Emmagatzematge "
"Persistent. ([[!tails_ticket 19728]])"

#. type: Plain text
#, no-wrap
msgid ""
"  Sometimes, *upgrading* the cryptographic parameters of the Persistent Storage\n"
"  was taking too long and made *unlocking* the Persistent Storage fail. We\n"
"  allowed the *upgrade* to take more time before reporting a failure.\n"
msgstr ""
"  De vegades, *actualitzar* els paràmetres criptogràfics de l'Emmagatzematge Persistent\n"
"  trigava massa i feia que el *desbloqueig* de l'Emmagatzematge Persistent fallés. Hem\n"
"  permès que l'*actualització* trigui més temps abans d'informar d'un error.\n"

#. type: Plain text
#, no-wrap
msgid ""
"  Please keep reporting errors using\n"
"  [[*WhisperBack*|doc/first_steps/whisperback]] if you have problems\n"
"  unlocking your Persistent Storage.\n"
msgstr ""
"  Si us plau, seguiu informant d'errors utilitzant\n"
"  [[*WhisperBack*|doc/first_steps/whisperback]] si teniu problemes\n"
"  desbloquejant el vostre Emmagatzematge Persistent.\n"

#. type: Bullet: '- '
msgid ""
"Fix the circuit view of *Tor Browser* that was sometimes not displayed.  ([[!"
"tails_ticket 19897]])"
msgstr ""
"S'ha corregit la vista del circuit del *Navegador Tor* que de vegades no es "
"mostrava. ([[!tails_ticket 19897]])"

#. type: Plain text
msgid ""
"For more details, read our [[!tails_gitweb debian/changelog "
"desc=\"changelog\"]]."
msgstr ""
"Per a més detalls, llegiu el nostre [[!tails_gitweb debian/changelog desc="
"\"registre de canvis\"]]."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"issues\">Known issues</h1>\n"
msgstr "<h1 id=\"issues\">Problemes coneguts</h1>\n"

#. type: Plain text
msgid "See the list of [[long-standing issues|support/known_issues]]."
msgstr ""
"Vegeu la llista de [[problemes de llarga durada|support/known_issues]]."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"get\">Get Tails 5.16</h1>\n"
msgstr "<h1 id=\"get\">Obtenir Tails 5.16</h1>\n"

#. type: Title ##
#, no-wrap
msgid "To upgrade your Tails USB stick and keep your persistent storage"
msgstr "Per actualitzar el vostre llapis USB de Tails i mantenir el vostre Emmagatzematge Persistent"

#. type: Plain text
msgid "- Automatic upgrades are available from Tails 5.0 or later to 5.16."
msgstr ""
"- Les actualitzacions automàtiques estan disponibles des de Tails 5.0 o "
"posterior fins a la versió 5.16."

#. type: Plain text
#, no-wrap
msgid ""
"  You can [[reduce the size of the download|doc/upgrade#reduce]] of future\n"
"  automatic upgrades by doing a manual upgrade to the latest version.\n"
msgstr ""
"  Podeu [[reduir la mida de la baixada|doc/upgrade#reduce]] de futures\n"
"  actualitzacions automàtiques fent una actualització manual a la darrera versió.\n"

#. type: Bullet: '- '
msgid ""
"If you cannot do an automatic upgrade or if Tails fails to start after an "
"automatic upgrade, please try to do a [[manual upgrade|doc/upgrade/#manual]]."
msgstr ""
"Si no podeu fer una actualització automàtica o si Tails no s'inicia després "
"d'una actualització automàtica, proveu de fer una [[actualització manual|doc/"
"upgrade/#manual]]."

#. type: Title ##
#, no-wrap
msgid "To install Tails on a new USB stick"
msgstr "Per instal·lar Tails en un nou llapis USB"

#. type: Plain text
msgid "Follow our installation instructions:"
msgstr "Seguiu les nostres instruccions d'instal·lació:"

#. type: Bullet: '  - '
msgid "[[Install from Windows|install/windows]]"
msgstr "[[Instal·lar des de Windows|install/windows]]"

#. type: Bullet: '  - '
msgid "[[Install from macOS|install/mac]]"
msgstr "[[Instal·lar des de macOS|install/mac]]"

#. type: Bullet: '  - '
msgid "[[Install from Linux|install/linux]]"
msgstr "[[Instal·lar des de Linux|install/linux]]"

#. type: Bullet: '  - '
msgid ""
"[[Install from Debian or Ubuntu using the command line and GnuPG|install/"
"expert]]"
msgstr ""
"[[Instal·lar des de Debian o Ubuntu mitjançant la línia d'ordres i GnuPG|"
"install/expert]]"

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"caution\"><p>The Persistent Storage on the USB stick will be lost if\n"
"you install instead of upgrading.</p></div>\n"
msgstr ""
"<div class=\"caution\"><p>L'Emmagatzematge Persistent del llapis USB es "
"perdrà si\n"
"instal·leu en comptes d'actualitzar.</p></div>\n"

#. type: Title ##
#, no-wrap
msgid "To download only"
msgstr "Per només baixar"

#. type: Plain text
msgid ""
"If you don't need installation or upgrade instructions, you can download "
"Tails 5.16 directly:"
msgstr ""
"Si no necessiteu instruccions d'instal·lació o actualització, podeu baixar "
"Tails 5.16 directament:"

#. type: Bullet: '  - '
msgid "[[For USB sticks (USB image)|install/download]]"
msgstr "[[Per a llapis USB (imatge USB)|install/download]]"

#. type: Bullet: '  - '
msgid "[[For DVDs and virtual machines (ISO image)|install/download-iso]]"
msgstr "[[Per a DVD i màquines virtuals (imatge ISO)|install/download-iso]]"
